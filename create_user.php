<?php
	if(isset($_POST['submit']))
	{
		require('uuid.php');
	
		$mysqli = new mysqli('localhost','opensim_user','shelivedtobe86','opensim');
		// Check if MySQL connection can be made
		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
			exit();
		}
	
		// Insert account details
		$uuid['user'] = uuid();

		$query['account'] = "INSERT INTO UserAccounts (PrincipalID, ScopeID, FirstName, LastName, Email, ServiceURLs, Created) 
		VALUES ('" . $uuid['user'] . "', 
				'00000000-0000-0000-0000-000000000000', 
				'" . $mysqli->real_escape_string($_POST['first_name']) . "', 
				'" . $mysqli->real_escape_string($_POST['last_name']) . "', 
				'" . $mysqli->real_escape_string($_POST['email']) . "', 
				'HomeURI= GatekeeperURI= InventoryServerURI= AssetServerURI=',
				" . time() . ")";
	
		$result['account'] = $mysqli->query($query['account']);
		if(!$result['account'])
		{
		   echo $mysqli->error;
		   exit();
		} else {
			echo "Account added!<br>";
		}
	
		// Insert authentication details
		$salted  = sprintf('%s', md5(uuid()));
		$hash    = md5(md5($_POST['password']) . ':' . $salted);

		$query['auth'] = "INSERT INTO auth (UUID, passwordHash, passwordSalt, webLoginKey) 
		VALUES ('" . $uuid['user'] . "', '" . $hash . "', '" . $salted . "', '00000000-0000-0000-0000-000000000000')";

		$result['auth'] = $mysqli->query($query['auth']);
		if(!$result['auth'])
		{
		   echo $mysqli->error;
		   exit();
		} else {
			echo "Authentication added!<br><br>";
		}
		
		// Insert inventory folder
		$uuid['textures'] = uuid();
		$uuid['sounds'] = uuid();
		$uuid['calling_cards'] = uuid();
		$uuid['landmarks'] = uuid();
		$uuid['clothing'] = uuid();
		$uuid['objects'] = uuid();
		$uuid['notecards'] = uuid();
		$uuid['my_inventory'] = uuid();
		$uuid['scripts'] = uuid();
		$uuid['body_parts'] = uuid();
		$uuid['trash'] = uuid();
		$uuid['photo_album'] = uuid();
		$uuid['lost_and_found'] = uuid();
		$uuid['animations'] = uuid();
		$uuid['gestures'] = uuid();

		$query['inventory_folders'] = "INSERT INTO inventoryfolders (folderName, type, version, folderID, agentID, parentFolderID) 
		VALUES 
			('Textures', 0, 1, '" . $uuid['textures'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Sounds', 1, 1, '" . $uuid['sounds'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Calling Cards', 2, 1, '" . $uuid['calling_cards'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Landmarks', 3, 1, '" . $uuid['landmarks'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Clothing', 5, 3, '" . $uuid['clothing'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Objects', 6, 1, '" . $uuid['objects'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Notecards', 7, 1, '" . $uuid['notecards'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('My Inventory', 9, 15, '" . $uuid['my_inventory'] . "', '" . $uuid['user'] . "', '00000000-0000-0000-0000-000000000000'),
			('Scripts', 10, 1, '" . $uuid['scripts'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Body Parts', 13, 5, '" . $uuid['body_parts'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Trash', 14, 1, '" . $uuid['trash'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Photo Album', 15, 1, '" . $uuid['photo_album'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Lost And Found', 16, 1, '" . $uuid['lost_and_found'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Animations', 20, 1, '" . $uuid['animations'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "'),
			('Gestures', 21, 1, '" . $uuid['gestures'] . "', '" . $uuid['user'] . "', '" . $uuid['my_inventory'] . "')";
	
		$result['inventory_folders'] = $mysqli->query($query['inventory_folders']);
		if(!$result['inventory_folders'])
		{
		   echo $mysqli->error;
		   exit();
		} else {
			echo "Inventory folders added!<br>";
		}
		
		// Insert inventory items
		$query['inventory_items'] = "INSERT INTO inventoryitems (assetID, assetType, inventoryName, inventoryNextPermissions, inventoryCurrentPermissions, invType, creatorID, inventoryBasePermissions, inventoryEveryOnePermissions, creationDate, groupOwned, flags, inventoryGroupPermissions, inventoryID, avatarID, parentFolderID) 
		VALUES 
			('" . uuid() . "', 5, 'Default Shirt', 2147483647, 2147483647, 18, '" . $uuid['user'] . "', '2147483647', '2147483647', " . time() . ", 0, 4, 2147483647, '" . uuid() . "', '" . $uuid['user'] . "', '" . $uuid['clothing'] . "'),
			('" . uuid() . "', 5, 'Default Pants', 2147483647, 2147483647, 18, '" . $uuid['user'] . "', '2147483647', '2147483647', " . time() . ", 0, 5, 2147483647, '" . uuid() . "', '" . $uuid['user'] . "', '" . $uuid['clothing'] . "'),
			('" . uuid() . "', 13, 'Default Hair', 2147483647, 2147483647, 18, '" . $uuid['user'] . "', '2147483647', '2147483647', " . time() . ", 0, 2, 2147483647, '" . uuid() . "', '" . $uuid['user'] . "', '" . $uuid['body_parts'] . "'),
			('" . uuid() . "', 13, 'Default Skin', 2147483647, 2147483647, 18, '" . $uuid['user'] . "', '2147483647', '2147483647', " . time() . ", 0, 1, 2147483647, '" . uuid() . "', '" . $uuid['user'] . "', '" . $uuid['body_parts'] . "'),
			('" . uuid() . "', 13, 'Default Eyes', 2147483647, 2147483647, 18, '" . $uuid['user'] . "', '2147483647', '2147483647', " . time() . ", 0, 3, 2147483647, '" . uuid() . "', '" . $uuid['user'] . "', '" . $uuid['body_parts'] . "'),
			('" . uuid() . "', 13, 'Default Shape', 2147483647, 2147483647, 18, '" . $uuid['user'] . "', '2147483647', '2147483647', " . time() . ", 0, 0, 2147483647, '" . uuid() . "', '" . $uuid['user'] . "', '" . $uuid['body_parts'] . "')";
	
		$result['inventory_items'] = $mysqli->query($query['inventory_items']);
		if(!$result['inventory_items'])
		{
		   echo $mysqli->error;
		   exit();
		} else {
			echo "Inventory items added!<br><br>";
		}		
		
		mysqli_close($mysqli);
		
		echo "Registratie voltooid.";
	} else {
		echo "Het formulier dient ingevuld te worden.";
	}
?>
