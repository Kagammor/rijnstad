<!DOCTYPE html>
<html>
	<head>
		<title>Rijnstad</title>
		
		<link href="./style.css" rel="stylesheet" type="textc/css" />
	</head>
	
	<body>
		<h1>Rijnstad: Account aanmaken</h1>
		
		<form method="post" action="create_user.php">
			<ul>
				<li>
					<ul>
						<li><b>Voornaam</b></li>
						<li><input type="text" name="first_name" maxlength="10" autocomplete="off"></li>
					</ul>
				</li>
				<li>
					<ul>
						<li><b>Achternaam</b></li>
						<li><input type="text" name="last_name" maxlength="12" autocomplete="off"></li>
					</ul>
				</li>
				<li>
					<ul>
						<li><b>Wachtwoord</b></li>
						<li><input type="password" name="password" autocomplete="off"></li>
					</ul>
				</li>
				<li>
					<ul>
						<li><b>E-mail</b></li>
						<li><input type="text" name="email"></li>
					</ul>
				</li>
				<li><input type="submit" name="submit" value="Registreer"></li>
			</ul>
		</form>
	</body>
</html>
