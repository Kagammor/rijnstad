<?php
	function uuid()
	{
		$uid = md5(uniqid('', TRUE));
	
		$uuid['1'] = substr($uid, 0, 8);
		$uuid['2'] = substr($uid, 8, 4);
		$uuid['3'] = substr($uid, 12, 4);
		$uuid['4'] = substr($uid, 16, 4);
		$uuid['5'] = substr($uid, 20, 12);
		
		return $uuid['1'] . '-' . $uuid['2'] . '-' . $uuid['3'] . '-' . $uuid['4'] . '-' . $uuid['5'];
	}
?>
